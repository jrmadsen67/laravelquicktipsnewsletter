#Codebyjeff Laravel Quick Tips Newsletter

About year or so ago, in a fit of frustration at never being able to remember when it was `where('name', 'Jeff')` versus `where('name', '=', 'Jeff')`, I sat down and started digging through the source code for the Collection class. And promptly discovered there were *two* Collection classes, and that was where my trouble was stemming from.

Aside from sorting out the answer to that question, I also discovered what a fascinating and useful thing Collections were. So much so, I first wrote a simple blog post about them (1) and then followed up with a short reference book (2). That became the foundation of further digging into the codebase to discover all sorts of hidden gems that were either undocumented or commonly overlooked, which in turn, became my weekly newsletter (http:/codebyjeff.com/newsletter).

The newsletter has steadily grown and built up enough tips that I thought it might be a good idea to set them all down in one place and make them easier to reference. Six months is not the longest time, I realize, but it seemed like it had enough weight to it that it was time to finally put this out.

So thank you all who have read my little snippets each week and patiently looked past my typos; welcome those of you who are just joining us; and let us look forward to another six months and more!

Thanks!

Jeff Madsen
@codebyjeff

1 - http://codebyjeff.com/blog/2015/05/stupid-collection-tricks-in-laravel

2 - https://leanpub.com/laravelcollectionsunraveled