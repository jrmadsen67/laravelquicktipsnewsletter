#Autoloading files

Hello Laravel Friends!

Do you like helper files? Isn't it nice not to have to put it all in a class that needs instantiating when you just want a handful of useful functions to use here and there? The problem is that using a framework makes it rather awkward and ugly to try to include single files.

Fortunately, you can also load them with composer! It's dead simple, as well.

First, create a helper file where ever you like. I usually make a Helpers directory for these, then call it something like "ViewHelper.php"

Then in your `composer.json` file, add it on to the "autoload" section (I'm displaying with the Laravel file as it comes with a fresh install; yours might be slightly different):

"autoload": {
    "classmap": [
        "database"
    ],
    "psr-4": {
        "App\\": "app/"
    },
    "files": [
        "app/Helpers/ViewHelpers.php"
    ]
},

That "files" section is a path to the file you want to include. Don't forget to `composer dump-autoload` and you're all set! You can add your own functions and they are available anywhere:

if (! function_exists('testHelper')) {
    function testHelper(){
        return 'test';
    }
}

Always a good idea to use that `function_exists` wrapper; php won't let you redeclare a function name in most cases, but better to handle it here.

That's it! Hope that helps!

