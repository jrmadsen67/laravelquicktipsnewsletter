#Authentication Changes

Hello, Laravel Friends!

Have you poked around in the new 5.3 Controllers/Auth directory yet? If you have, you'll notice that the controllers have been rearranged a little bit. Formerly there were two - `AuthController`, which took care of Login and Register functionalities, and `PasswordController` which handled the password resets and emails.

In 5.3 Taylor has chosen to break these up into four classes: `LoginController` and `RegisterController`, and the `ForgotPasswordController` and `ResetPasswordController`. All of them still rely heavily on traits, where the real logic has always been. I like this at first blush; it makes things a bit more modular and focused.

One other change that actually is the reason I chose this subject for this this week is the new:

public function username()
{
    return 'email';
}

found on `AuthenticatesUsers`. In the past was the function:

public function loginUsername()
{
    return property_exists($this, 'username') ? $this->username : 'email';
}

which could be set with a property on the controller (ie, `protected $username = X`). Now, to change this field you want to overwrite the `username()` function directly, returning the name of your desired field as a string.

(You may also notice that `getCredentials()` has been renamed to `credentials()`, but it is otherwise exactly the same.)


Hope that helped!