#Inserting Related Models

Hello, Laravel Friends!

Have you ever wondered what the "new" was all about when working with Eloquent Models? You see it in `new User`, of course, but you can also find it methods like `firstOrNew` or just `new User(['username' => 'codebyjeff'])`. What is the point of this?

The short answer is, this creates an instance of the object but does not fire off a save() command to the insert the record in the database. That's why you have to remember to $user->save() at the end. But what's the point of the extra step? Why not just use User::create([]) and do it all at once?

You may come up with other reasons while you're working, but the one I want to point out today is Inserting and Updating related models via Eloquent. Something I see that is very common, and I think a mistake, is to ignore the fact that you are using an ORM and instead set up your relationships manually.

For example:

    new Post::create(['user_id' => 1, 'title' => 'The title']);

This is bad for a couple of reasons. First - isn't that the job of the ORM? Of course it is! Compare:

    $post = Post::create(['title' => 'The title']);

    User::find(1)->posts()->save($post);

Not only do I let me ORM do the work, but it gives me added security of checking that I've got the right User and they exist. (Better is a try/catch with findOrFail, for example).

BUT!! I have a problem, which ties us back in to my original point. If you have set up your foreign keys correctly, this second method will fail! Why? Because you are tying to `create` the Post without the user_id, which will be required. That's why we simply make a new instance without saving it.


    $post = new Post(['title' => 'The title']);

    User::find(1)->posts()->save($post);


In this case, Laravel will add the `user_id` itself, based on your relationship info. There are a few ways of making a new instance without saving (using factories, for example, which we'll touch on when we look into testing more), but this is the most straightforward.

Have a look here https://laravel.com/docs/5.2/eloquent-relationships#inserting-related-models for this and other ways of attaching and detaching related models.



Read it Monday, use it by Friday!

Jeff