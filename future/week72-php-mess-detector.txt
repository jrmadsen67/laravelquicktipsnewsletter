#PHP Mess Detector

Hello Friends!

A while ago Taylor wrote a short piece on Medium about measuring Laravel's code complexity, especially as compared to other frameworks. https://medium.com/@taylorotwell/measuring-code-complexity-64356da605f9#.w3znzun91 I made a few notes at the time and finally pulled up a tool I'm sure you've all heard of, but perhaps have never tried - PHP Mess Detector. https://phpmd.org

PHP MD is a "code sniffer" created by PHP Unit people such as Volker Dusch and Sebastian Bergmann (and a whole list of other smart people - don't mean to sell them short, but it is a long list). If you've used a tool like Code Scrutinizer you understand what it is; it looks for unused variables, code that violates norms or seems overly inefficient, etc. PHP MD, however, is a locally run library that you can composer install and set up to run exactly when and how you want to.

To install it, just use `composer require phpmd/phpmd`. Running `./vendor/bin/phpmd` will give you a man page, but you'll want to poke around the documentation when you play with it to see all the options available. Once it is running, there are several ways to configure; try running this on a new Laravel install, for example:

`./vendor/bin/phpmd app/ html cleancode --reportfile phpmd.html`

This is not a Laravel tool, but rather a PHP one, which means you will see it complain a bit about things like the use of statics in Facades. A nice thing, however, is that it is local and won't stop you from pushing to the repo if it doesn't like something, in the way that Scrutinizer and some others are known to do. You can decide which directories you want reviewed, to help keep this speedy and focused on what you are working on. Since it is a local script, you can write your own automation, as well.

As a really nice bonus - if you are using PhpStorm, it is already integrated! (NOTE: I have NOT ever tested this, which I don't usually like to post, but here are some instructions on how to set it up: https://confluence.jetbrains.com/display/PhpStorm/PHP+Mess+Detector+in+PhpStorm)

While you might not want to follow all the advice it gives, for those of us who typically work alone on most projects it is a simple way to get someone to look over our shoulder and give a little confidence our code doesn't have too many "smells" or embarrassments.

Hope that helps!