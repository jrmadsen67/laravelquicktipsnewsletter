#Rule Objects

Hello Laravel Friends!

Good chance you've heard by now, but there is some new excitement with Validation rules. Remember how nasty is was to remember rules like Unique, when you wanted to exclude the current user id so you could do an update with the same email? No more fuss!

Starting in v5.3.17 there is a new Rule Object for Unique and Exists rules. Where you used to have:

$validator = Validator::make(
    ['username' => 'codebyjeff'],
    ['username' => 'unique:users,username']
);

You now have:

$validator = Validator::make(
    ['username' => 'codebyjeff'],
    ['username' => [
        Rule::unique('users')->ignore($user->id)
    ]]
);

That doesn't seem like so much, but open up one of these rules and see what else comes with the object. You can find all of them in the Illuminate\Validation directory.

First you'll see that Rule.php just makes some static calls to simplify things; Rules\Exists and Rules\Unique are the actual objects. Looking in either of them shows us that there are a whole series of Eloquent-like functions that can be chained to give us further precision. For example, we could string a list of (nonsense in this case) checks such as:

$validator = Validator::make(
    ['username' => 'codebyjeff'],
    ['username' => [
        Rule::unique('users')->whereNot('is_admin', 1)->where(function() use($account_id){
            return $account_id > 10;
        })->ignore($user->id)
    ]]
);

You can even run a separate Eloquent query inside a where() (or using()) function! You can see there's both a lot of flexibility and much easier to work with functionality. A great improvement, if you ask me.

Hope that helps!