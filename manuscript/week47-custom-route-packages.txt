#Custom route packages

Hello, Laravel Friends! Welcome to Laravel 5.3!

I'm sure you've heard about the changes to the way routes are done. While more of the buzz seems to be around the fact that the file was moved outside the /app directory, the change that has me excited is the ability to break that giant, messy file into multiple files and load all or none of them as you need.

Let's do a whirlwind tour of this and also create our own example, as it isn't very difficult.

We have a `RouteServiceProvider` tasked with loading the route files. It has functions `mapWebRoutes` and `mapApiRoutes` which, in these cases, create route groups with middleware and namespacing. These then load the appropriate file from routes/web.php or routes/api.php, which you can see are nothing more than the old routes.php file renamed.

Why don't we make our own and then we can spot a key difference.

`artisan make:provider MyRouteProvider` gets us the service provider. Remember to register in config/app.php, and add the following:

`use Illuminate\Support\Facades\Route;`

public function register()
{
    Route::group([ ], function ($router) {
        require base_path('routes/test.php');
    });
}


Then a routes/test.php with:

Route::get('/test', function () {
    return 'testing';
});

will do the rest. Go to "http://localhost/test" (or whatever your working url is) and you should see "testing" echoed to the page.

Great! Except...ours looks a little different. `RouteServiceProvider` is calling these loading functions from `map()`, but if I look at the ServiceProvider we're extending from, there's no such function.

Ah! There's a new core service provider being used for this one: `use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;` Look at *that* file and we see some interesting things.

The `boot` is where all the action is happening. You can see it is a simple case to read through; namespacing is set up and then we make use of the new Route Caching feature if we've chosen to use that (https://laravel.com/docs/5.3/controllers#route-caching).

Then the `map()` is called, and the rest follows the expected workflow.

Not only is this wonderful for keeping our route groups sane, but it also mean we can bundle our routes with our packages - we just load them via the package service provider! Have a look at the new Passport Oauth package to see a great example of how Taylor has done just that.



Hope that helps!